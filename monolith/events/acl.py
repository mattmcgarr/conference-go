import requests
from .keys import PEXEL_KEY, OPEN_WEATHER_API_KEY


def get_location_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    response = requests.get(url, headers={'Authorization': PEXEL_KEY})
    return response.json()["photos"][0]["url"]


def get_weather_data(city, state):
    params = {"q": f"{city},{state}, US", "appid": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()

    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]

    except (KeyError, IndexError):
        return None

    # get weather from location we got above

    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }

    url = 'https://api.openweathermap.org/data/2.5/weather'
    response = requests.get(url, params=params)
    json_resp = response.json()

    try:
        temp = json_resp["main"]["temp"]
        description = json_resp["weather"][0]["description"]

        return {"temp": temp, "description": description}

    except (KeyError, IndexError):
        return None
