from django.http import JsonResponse
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    ConferenceDetailEncoder,
    ConferenceListEncoder,
    LocationDetailEncoder,
    LocationListEncoder,
)
from .acl import get_location_photo, get_weather_data


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):


    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,)
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        Conference.objects.create(**content)
        return JsonResponse({"conferences": conferences}, encoder=ConferenceListEncoder)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Conference id"},
            status=404,
        )

    if request.method == "GET":
        weather = get_weather_data(conference.location.city, conference.location.state.abbreviation)
        return JsonResponse({"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        Conference.objects.filter(id=id).update(**content)


        return JsonResponse(conference,encoder=ConferenceDetailEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder)
    else:
        # covert json formatted string in request.body
        content = json.loads(request.body)

        # get the State Object and put it in the content dict
        # need to do this because state is a foreign key
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse({"message": "Invalid state abbreviation"},
                                status=400,)

        # use city/state abbreviation in the content dictionary
        # to call the get_location_photo function
        content["location_url"] = get_location_photo(content["city"], content["state"])

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    try:
        location = Location.objects.get(id=id)
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Location id"},
            status=404,
        )

    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        if "state" in content:

            try:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
            except State.DoesNotExist:
                return JsonResponse({"message": "Invalid state abbreviation"},
                                    status=400,)

        location = Location.objects.filter(id=id).update(**content)

        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
