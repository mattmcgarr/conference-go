from django.http import JsonResponse
from .models import Attendee
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from attendees.models import ConferenceVO
from .encoders import AttendeeDetailEncoder, AttendeeListEncoder



@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        # retrieves a list of attendees who are associated with conference
        # based on the conference id
        # filter method returns a queryset
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        # JsonResponse takes the above queryset and turns it into json
        # which is specified by the encoder
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else: #POST request
        # received json data from post is decoded into a python dictionary
        content = json.loads(request.body)

        # get conference id and put it in the content dict
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            # retrieves conference by id from database
            conference = ConferenceVO.objects.get(import_href=conference_href)
            # adds retrieved conference object to the "content" dictionary
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"}, status=400)

        # creates a new attendee object in database using info from content dictionary
        # ** unpacks content dictionary which allows the contents
        # of the dictionary to be passed as keyword arguemtns to the create method
        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee, encoder=AttendeeListEncoder, safe=False)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid attendee id"},
            status=404,
        )

    if request.method == "GET":
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=404,
            )

        Attendee.objects.filter(id=pk).update(**content)

        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
