"""attendees_bc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from attendees.api_views import api_list_attendees, api_show_attendee

urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/", include("attendees.api_urls")),
    path("attendees/", api_list_attendees, name="api_create_attendees"),
    path("conferences/<int:conference_vo_id>/attendees/",
         api_list_attendees, name="api_list_attendees",),
    path("attendees/<int:pk>/", api_show_attendee, name="api_show_attendee"),
]
